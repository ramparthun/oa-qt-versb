#include <iostream>
#include <QDirIterator>
#include <QDateTime>
int main(int argc, char **argv)
{
    if (argc < 2) {
        std::cout << "No folder given" << std::endl;
        return 1;
    }
    if(argc<3){
        std::cout << "No number for folder given" << std::endl;
        return 1;
    }
    QString seged=argv[2];
    bool ok;
    int num=seged.toInt(&ok,10);
    if(!ok){
        std::cout << "The second parameter is not a number" << std::endl;
        return 1;
    }
    if(ok&&(num<0)){
        std::cout << "Folder number is not valid" << std::endl;
        return 1;
    }
    QDirIterator dirIterator(
        argv[1],
        QDir::NoDotAndDotDot | QDir::Files,
        QDirIterator::Subdirectories
    );
    int i=0;
    QString dir="";
    while (dirIterator.hasNext()&&i<num) {
        QString size;
        dirIterator.next();
        if(dirIterator.fileInfo().path()!=dir){
            std::cout<<"Directory: "<<dirIterator.fileInfo().absolutePath().toStdString()<<std::endl;//directory
            dir=dirIterator.fileInfo().path();
        }
        qint64 seged=dirIterator.fileInfo().size();
        size.setNum(seged/1024);
        size+=" kB";
        size=size.leftJustified(10,' ');
        QDateTime t2 = dirIterator.fileInfo().created();
        QString date = t2.toString("yyyy-mm-dd hh:mm");
        date=date.leftJustified(30,' ');
        std::cout <<"\t"<< size.toStdString()<<date.toStdString()<<dirIterator.fileInfo().fileName().toStdString()<<std::endl;//line
        i++;
    }
    return 0;
}

